This is a fork from upstream XTF to add ARM support; the aim is to merge
ARM support back into upstream XTF at the first opportunity. Upstream
XTF is located here: https://xenbits.xenproject.org/git-http/xtf.git

The main branch with ARM support is "xtf-arm".


CONTRIBUTING
------------

Send patches to xen-devel@lists.xenproject.org, CC the maintainers.
Add [XTF-ARM] to the subject line.


MAINTAINERS
-----------

Michal Orzel <michal.orzel@amd.com>
