/**
 * @file arch/arm/include/arch/extable.h
 *
 * Common arm exception table helper functions.
 */
#ifndef XTF_ARM64_EXTABLE_H
#define XTF_ARM64_EXTABLE_H

#endif /* XTF_ARM_EXTABLE_H */

/*
 * Local variables:
 * mode: C
 * c-file-style: "BSD"
 * c-basic-offset: 4
 * tab-width: 4
 * indent-tabs-mode: nil
 * End:
 */
